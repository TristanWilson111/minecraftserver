# DockerMinecraft
A Docker Image for Minecraft
(Spigot and Forge)

* Java program to start and stop the server regular
* Only your userdata in the shared folder

Links to Dockerhub:
* Spigot: https://hub.docker.com/r/marcermarc/spigot/
* Forge: https://hub.docker.com/r/marcermarc/forge/

you can use the docker-compose files in this repos

###########

Original Docker image: https://hub.docker.com/r/marcermarc/spigot
Repo: https://github.com/marcermarc/DockerMinecraft

Needed to remove user permissions and use root in dockerfile, required changing docker-compose as well to use build instead of image